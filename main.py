import base64
import getopt
import random
import string
import struct
import sys

from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from github import GitHub
from pyasn1.codec.der import encoder as der_encoder
from pyasn1.type import univ

gh = GitHub()


def encrypt_message(key, message):
    """
    Encrypts a message using an RSA public key
    :param key: RSA Public Key as PEM
    :param message: Message to be encrypted
    :return: Ciphertext
    """
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)
    ciphertext = cipher.encrypt(message)
    return base64.b64encode(ciphertext)


def decrypt_message(key, ciphertext):
    """
    Decryptes a message using an RSA private key
    :param key: RSA Private Key as PEM
    :param ciphertext: Message to be decrypted
    :return: Plaintext
    """
    key = RSA.importKey(key)
    cipher = PKCS1_OAEP.new(key)
    message = cipher.decrypt(base64.b64decode(ciphertext))

    return message


def random_string(length):
    """
    Generates a random string of letters
    :param length: Length of the string
    :return: Random string
    """
    return ''.join(random.choice(string.lowercase) for i in range(length))


def convert_key(key):
    """
    Converts key in OpenSSH format to PEM format
    :param key: OpenSSH format key
    :return: PEM format key
    """

    keyfields = key.split(None)
    if len(keyfields) < 3:
        # there might not be a comment, so pad it
        keyfields.append("")
    keytype, keydata, keycomment = keyfields

    if keytype != 'ssh-rsa':
        sys.stderr.write("%s: key type does not appear to be ssh-rsa\n")
        sys.exit(1)

    keydata = base64.b64decode(keydata)

    parts = []
    while keydata:
        # read the length of the data
        dlen = struct.unpack('>I', keydata[:4])[0]

        # read in <length> bytes
        data, keydata = keydata[4:dlen + 4], keydata[4 + dlen:]

        parts.append(data)

    e_val = eval('0x' + ''.join(['%02X' % struct.unpack('B', x)[0] for x in parts[1]]))
    n_val = eval('0x' + ''.join(['%02X' % struct.unpack('B', x)[0] for x in parts[2]]))

    bitstring = univ.Sequence()
    bitstring.setComponentByPosition(0, univ.Integer(n_val))
    bitstring.setComponentByPosition(1, univ.Integer(e_val))

    bitstring = der_encoder.encode(bitstring)

    bitstring = ''.join([('00000000' + bin(ord(x))[2:])[-8:] for x in list(bitstring)])

    bitstring = univ.BitString("'%s'B" % bitstring)

    pubkeyid = univ.Sequence()
    pubkeyid.setComponentByPosition(0, univ.ObjectIdentifier('1.2.840.113549.1.1.1'))  # == OID for rsaEncryption
    pubkeyid.setComponentByPosition(1, univ.Null(''))

    pubkey_seq = univ.Sequence()
    pubkey_seq.setComponentByPosition(0, pubkeyid)
    pubkey_seq.setComponentByPosition(1, bitstring)

    pem = "-----BEGIN RSA PUBLIC KEY-----\n"
    if keycomment:
        pem += "X-Comment: %s\n" % keycomment
    base64.MAXBINSIZE = (64 // 4) * 3  # this actually doesn't matter, but it helped with comparing to openssl's output
    pem += base64.encodestring(der_encoder.encode(pubkey_seq))
    pem += '-----END RSA PUBLIC KEY-----'

    return pem


def get_public_key(username):
    """
    Retrieves the first public key for a specified user from GitHub
    :param username: Username of the user
    :return: PEM format Public Key
    """
    keys = gh.users(username).keys.get()

    return convert_key(keys[0]['key'])


def create_gist(data, filename=None):
    """
    Creates a gist on Github
    :param data: Data for the gist
    :param filename: filename
    :return: N/A
    """
    description = ""
    public = True
    if not filename:
        filename = random_string(10)
    files = {}
    files[filename] = {}
    files[filename]['content'] = data

    gist = gh.gists.post(files=files, description=description, public=public)
    return gist.html_url


def main():
    username = ''
    key = ''
    filename = ''
    method = True

    try:
        opts, args = getopt.getopt(sys.argv[1:], "u:f:deh",
                                   ["username=", "key=", "filename=", "decrypt", "encrypt", "help"])

    except getopt.GetoptError:
        print "cs6903.py -e -u <GitHub username> -f <Filename>"
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print "cs6903.py -e -u <GitHub username> -f <Filename>"
            sys.exit()
        elif opt in ("-u", "--username"):
            username = arg
        elif opt in ("-f", "--filename"):
            filename = arg

    data = open(filename, 'rb').read()
    pk = get_public_key(username)
    msg = encrypt_message(pk, data)
    url = create_gist(msg, filename)

    print "Your encrypted gist was uploaded here: %s" % url


if __name__ == '__main__':
    main()
